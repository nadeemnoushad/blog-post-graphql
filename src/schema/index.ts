import { GraphQLSchema, GraphQLObjectType } from "graphql";
import { CREATE_POST, DELETE_POST, UPDATE_POST } from "./mutations/Post";
import { GET_ALL_POSTS } from "./queries/Post";
import { LOGIN, REGISTER } from "./mutations/User";

const RootQuery = new GraphQLObjectType({
  name: "RootQuery",
  fields: {
    getAllPost: GET_ALL_POSTS,
  },
});

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: {
    createPost: CREATE_POST,
    deletePost: DELETE_POST,
    updatePost: UPDATE_POST,
    register: REGISTER,
    login: LOGIN,
  },
});

export const schema = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation,
});
