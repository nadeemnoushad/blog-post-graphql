import { GraphQLString } from "graphql";
import { PostType } from "../typedef/Post";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

export const CREATE_POST = {
  type: PostType,
  args: {
    title: { type: GraphQLString },
    body: { type: GraphQLString },
    date: { type: GraphQLString },
    user_id: { type: GraphQLString },
  },
  async resolve(parent: any, args: any) {
    const post = await prisma.post.create({ data: args });
    return post;
  },
};

export const DELETE_POST = {
  type: PostType,
  args: {
    id: { type: GraphQLString },
  },

  async resolve(parent: any, args: any) {
    const id = args.id;
    await prisma.post.delete({ where: { id } });
    return args;
  },
};

export const UPDATE_POST = {
  type: PostType,
  args: {
    id: { type: GraphQLString },
    title: { type: GraphQLString },
    body: { type: GraphQLString },
    date: { type: GraphQLString },
  },

  async resolve(parent: any, args: any) {
    const id = args.id;
    await prisma.post.update({ where: { id }, data: args });
    return args;
  },
};
