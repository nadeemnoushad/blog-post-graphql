import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

export default class Authentication {
  model: any;
  jwt_secret: string;

  constructor(model: any) {
    this.model = model;
    this.jwt_secret = process.env.JWT_SECRET as string;
  }

  async register(data: any) {
    const user_exist = await this.model.findUnique({
      where: { email: data.email },
    });
    if (user_exist)
      console.log("An account is already associated with this email");

    const salt = await bcrypt.genSalt(10);
    const hashed_password = await bcrypt.hash(data.password, salt);

    data.password = hashed_password;
    const user = await this.model.create({
      data: data,
    });
    return user;
  }

  async login(email: string, password: string) {
    const user = await this.model.findFirst({ where: { email } });
    if (!user) {
      throw "Invalid email or password";
    }

    const isValid = await bcrypt.compare(password, user.password);
    if (!isValid) {
      throw "Invalid email or password";
    }

    const token = jwt.sign(
      { id: user.id, email, name: user.name },
      process.env.JWT_SECRET as string
    );
    return token;
  }
}
