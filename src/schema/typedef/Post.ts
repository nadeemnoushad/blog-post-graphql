import { GraphQLID, GraphQLString, GraphQLObjectType } from "graphql";

export const PostType = new GraphQLObjectType({
  name: "Post",
  fields: {
    id: { type: GraphQLID },
    title: { type: GraphQLString },
    body: { type: GraphQLString },
    date: { type: GraphQLString },
  },
});
