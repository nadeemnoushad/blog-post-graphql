import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
declare namespace Express {
  export interface Request {
    user: any;
  }
}

export function authMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const token = req.headers.authorization;
  console.log(token);

  if (token) {
    try {
      const decoded = jwt.verify(token, "secret");
      // req.user = decoded;
      next();
    } catch (err) {
      res.status(401).json({ error: "Invalid token" });
    }
  } else {
    res.status(401).json({ error: "No token provided" });
  }
}
