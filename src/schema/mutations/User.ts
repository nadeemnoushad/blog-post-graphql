import { GraphQLString } from "graphql";
import { UserType } from "../typedef/User";
import { PrismaClient } from "@prisma/client";
import Authentication from "../../helpers/class/authentication";
import { authMiddleware } from "../../middleware/authorize";

const prisma = new PrismaClient();
const auth = new Authentication(prisma.user);

export const REGISTER = {
  type: UserType,
  args: {
    name: { type: GraphQLString },
    email: { type: GraphQLString },
    password: { type: GraphQLString },
  },

  async resolve(parent: any, args: any) {
    const user = await auth.register(args);
    return user;
  },
};

export const LOGIN = {
  type: UserType,
  args: {
    email: { type: GraphQLString },
    password: { type: GraphQLString },
  },

  async resolve(parent: any, args: any) {
    const token = await auth.login(args.email, args.password);
    return { token };
  },
};
