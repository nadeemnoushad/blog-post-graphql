import { PrismaClient } from "@prisma/client";
import { PostType } from "../typedef/Post";
import { GraphQLList } from "graphql";

const prisma = new PrismaClient();

export const GET_ALL_POSTS = {
  type: new GraphQLList(PostType),
  async resolve() {
    const posts = await prisma.post.findMany();
    return posts;
  },
};
