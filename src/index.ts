import express from "express";
import cors from "cors";
import { graphqlHTTP } from "express-graphql";
import { schema } from "./schema";

const server = () => {
  const app = express();

  app.use(cors());

  app.use(
    "/graphql",
    graphqlHTTP({
      schema,
      graphiql: true,
    })
  );

  app.listen(3000, () => console.log(`Server running on PORT 3000`));
};

server();
